-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2020 at 01:46 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistem_penjualan_2017110069`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `Kd_Barang` int(3) NOT NULL,
  `Nama_Barang` varchar(40) NOT NULL,
  `Status` varchar(10) NOT NULL,
  `Kategori` varchar(20) NOT NULL,
  `Kd_Brand` int(3) NOT NULL,
  `Nama_Brand` varchar(30) NOT NULL,
  `Soh` int(3) NOT NULL,
  `Harga` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`Kd_Barang`, `Nama_Barang`, `Status`, `Kategori`, `Kd_Brand`, `Nama_Brand`, `Soh`, `Harga`) VALUES
(3, 'GUARDIAN KIDS HEAD TOE ORANGE 800ML', 'Active', 'Baby Care', 1, 'House Brand', 4, 67000),
(4, 'GUARDIAN KIDS HEAD TOE STRAWBE 800ML', 'Active', 'Baby Care', 1, 'House Brand', 3, 67000),
(5, 'OS SHWR MINT&TEA TREE BT250ML', 'Active', 'Personal Care', 2, 'Unilever', 2, 35000),
(6, 'DOVE B/W GO FRESH TOUCH BTL 200ML', 'Active', 'Personal Care', 2, 'Unilever', 5, 30000),
(11, 'GUARDIAN BABY LIQUID BATH 800M', 'Active', 'Baby Care', 0, 'House Brand', 2, 65000),
(12, 'SEBAMED BABY SHAMPOO 250ML', 'Active', 'Baby Care', 0, 'Unilever', 4, 35000),
(13, 'SEBAMED LIP BALM BABY 4,8GR', 'Active', 'Baby Care', 0, 'Unilever', 9, 45000),
(14, 'CETAPHIL BABY MOIST BATH&WASH 230MLC', 'Active', 'Baby Care', 0, 'Unilever', 2, 55000),
(15, 'LACTACYD LIQUID BABY 250ML', 'Active', 'Baby Care', 0, 'Unilever', 4, 35500),
(16, 'GUARDIAN COTTON BUDS 100\'S', 'Active', 'Baby Care', 0, 'House Brand', 10, 10000),
(17, 'ZWITSAL BATH NAT HB 300ML', 'Active', 'Baby Care', 0, 'Png', 12, 25500),
(20, 'STERO-BAG HAND STERLZR 40ML', 'Active', 'Personal Care', 0, 'Png', 21, 21000),
(23, 'CLOSEUP D/ACTION GEL TOOTHPASTE 160G', 'Active', 'Baby Care', 0, 'House Brand', 24, 22500),
(24, 'CEREBROFORT M/GUMMY ORNGE20G ', 'Active', 'Health Care', 0, 'Unilever', 120, 22500),
(25, 'SAKATONIC ABC ANGGUR 30S', 'Active', 'Baby Care', 0, 'House Brand', 8, 35500),
(26, 'IM BOOST SIRUP 120ML', 'Active', 'Health Care', 0, 'Wings', 100, 45500),
(27, 'EYEVIT TABLET 6\'S', 'Delisted', 'Health Care', 0, 'Png', 2, 40500),
(28, 'Vape', 'Active', 'Baby Care', 0, 'House Brand', 2, 44),
(29, 'pod', 'Active', 'Baby Care', 0, 'House Brand', 2, 1111);

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `Kd_Brand` int(3) NOT NULL,
  `Nama_Brand` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`Kd_Brand`, `Nama_Brand`) VALUES
(1, 'House Brand'),
(2, 'Unilever'),
(3, 'PnG'),
(4, 'Wings'),
(5, 'Wings'),
(6, 'Nature Health'),
(22, 'BlackMore');

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `Tanggal` date NOT NULL,
  `Kd_Penjualan` int(3) NOT NULL,
  `Kd_Barang` int(3) NOT NULL,
  `Nama_Barang` varchar(40) NOT NULL,
  `Qty` int(3) NOT NULL,
  `Harga` int(7) NOT NULL,
  `Sub_Total` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jual`
--

CREATE TABLE `jual` (
  `Kode_Transaksi` char(5) NOT NULL,
  `Tanggal` date NOT NULL,
  `Kode_Barang` char(3) NOT NULL,
  `Nama_Barang` varchar(35) NOT NULL,
  `Harga` int(6) NOT NULL,
  `Jumlah` int(3) NOT NULL,
  `Subtotal` int(7) NOT NULL,
  `Total` int(7) NOT NULL,
  `Bayar` int(7) NOT NULL,
  `Kembali` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jual`
--

INSERT INTO `jual` (`Kode_Transaksi`, `Tanggal`, `Kode_Barang`, `Nama_Barang`, `Harga`, `Jumlah`, `Subtotal`, `Total`, `Bayar`, `Kembali`) VALUES
('KT001', '2020-04-15', '6', 'DOVE B/W GO FRESH TOUCH BTL 200ML', 30000, 2, 60000, 60000, 70000, 10000),
('KT002', '2020-04-15', '26', 'IM BOOST SIRUP 120ML', 45500, 2, 91000, 158500, 160000, 1500),
('KT002', '2020-04-15', '24', 'CEREBROFORT M/GUMMY ORNGE20G ', 22500, 3, 67500, 158500, 160000, 1500),
('KT003', '2020-04-15', '6', 'DOVE B/W GO FRESH TOUCH BTL 200ML', 30000, 2, 60000, 60000, 70000, 10000),
('KT004', '2020-04-15', '12', 'SEBAMED BABY SHAMPOO 250ML', 35000, 2, 70000, 135000, 150000, 15000),
('KT004', '2020-04-15', '11', 'GUARDIAN BABY LIQUID BATH 800M', 65000, 1, 65000, 135000, 150000, 15000);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `Id` int(11) NOT NULL,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`Id`, `UserName`, `Password`, `Level`) VALUES
(99, 'Sendy_Hermawan', 'Bismillah', ''),
(1, 'admin', 'admin', 'low');

-- --------------------------------------------------------

--
-- Table structure for table `recived`
--

CREATE TABLE `recived` (
  `No_Recived` int(3) NOT NULL,
  `Kd_Supliyer` int(3) NOT NULL,
  `Kd_Barang` int(3) NOT NULL,
  `Nama_Barang` int(35) NOT NULL,
  `Soh` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `return`
--

CREATE TABLE `return` (
  `Kode_Return` int(3) NOT NULL,
  `Tanggal` date NOT NULL,
  `Kode_Barang` int(3) NOT NULL,
  `Nama_Barang` varchar(35) NOT NULL,
  `Jumlah` int(3) NOT NULL,
  `Harga` int(7) NOT NULL,
  `Keterangan` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supliyer`
--

CREATE TABLE `supliyer` (
  `Kd_Supliyer` int(3) NOT NULL,
  `Nama_Supliyer` varchar(25) NOT NULL,
  `Hp` varchar(13) NOT NULL,
  `Alamat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supliyer`
--

INSERT INTO `supliyer` (`Kd_Supliyer`, `Nama_Supliyer`, `Hp`, `Alamat`) VALUES
(1, 'm150', '022898989', 'Kebaktian'),
(2, 'm928', '323232', 'dsfdsfsdfsd'),
(3, 'ss211', '9880912', 'kiaracondong'),
(4, 'A123', '02998876', 'Babakan Sari');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`Kd_Barang`),
  ADD KEY `Kd_Brand` (`Kd_Brand`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`Kd_Brand`);

--
-- Indexes for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  ADD PRIMARY KEY (`Kd_Penjualan`);

--
-- Indexes for table `recived`
--
ALTER TABLE `recived`
  ADD PRIMARY KEY (`No_Recived`);

--
-- Indexes for table `return`
--
ALTER TABLE `return`
  ADD PRIMARY KEY (`Kode_Return`);

--
-- Indexes for table `supliyer`
--
ALTER TABLE `supliyer`
  ADD PRIMARY KEY (`Kd_Supliyer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `Kd_Barang` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `Kd_Brand` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `detail_penjualan`
--
ALTER TABLE `detail_penjualan`
  MODIFY `Kd_Penjualan` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recived`
--
ALTER TABLE `recived`
  MODIFY `No_Recived` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supliyer`
--
ALTER TABLE `supliyer`
  MODIFY `Kd_Supliyer` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
