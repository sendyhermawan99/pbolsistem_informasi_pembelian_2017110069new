/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem_informasi_pembelian_2017110069;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author SENARIYUS
 */
public class DbDetailPenjualan {

    String Tanggal;
    int KdPenjualan;
    int KdBarang;
    String NamaBarang;
    int Qty;
    int Harga;
    int SubTotal;

    public String getTanggal() {
        return Tanggal;
    }

    public void setTanggal(String Tanggal) {
        this.Tanggal = Tanggal;
    }

    public int getKdPenjualan() {
        return KdPenjualan;
    }

    public void setKdPenjualan(int KdPenjualan) {
        this.KdPenjualan = KdPenjualan;
    }

    public int getKdBarang() {
        return KdBarang;
    }

    public void setKdBarang(int KdBarang) {
        this.KdBarang = KdBarang;
    }

    public String getNamaBarang() {
        return NamaBarang;
    }

    public void setNamaBarang(String NamaBarang) {
        this.NamaBarang = NamaBarang;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int Qty) {
        this.Qty = Qty;
    }

    public int getHarga() {
        return Harga;
    }

    public void setHarga(int Harga) {
        this.Harga = Harga;
    }

    public int getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(int SubTotal) {
        this.SubTotal = SubTotal;
    }
    
    
    public boolean insert(){
    boolean berhasil=false;
    Koneksi con=new Koneksi();
    try {
         con.bukaKoneksi();
         con.preparedStatement = con.dbkoneksi.prepareStatement
        ("Insert into detail_penjualan"+"(Tanggal, Kd_Penjualan, Kd_Barang, Nama_Barang, Qty, Harga, Sub_Total) values (?,?,?,?,?,?,?)");
                     con.preparedStatement.setString(1, this.Tanggal);
                     con.preparedStatement.setInt(2, this.KdPenjualan);
                     con.preparedStatement.setInt(3, this.KdBarang);
                     con.preparedStatement.setString(4, this.NamaBarang);
                     con.preparedStatement.setInt(5, this.Qty);
                     con.preparedStatement.setInt(6, this.Harga);
                     con.preparedStatement.setInt(7, this.SubTotal);
                     con.preparedStatement.executeUpdate();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }

    public Vector Load(){
        try{
            Vector tableData = new Vector();
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery
        ("Select Tanggal, Kd_Penjualan, Kd_Barang, Nama_Barang, Qty, Harga, Sub_Total from detail_penjualan");
        int i=1; 
        while (rs.next()){
            Vector <Object> row = new Vector <Object>();
            row.add(i);
            row.add(rs.getString("Tanggal"));
            row.add(rs.getInt("Kd_Penjualan"));
            row.add(rs.getInt("Kd_Barang"));
            row.add(rs.getString("Nama_Barang"));
            row.add(rs.getInt("Qty"));
            row.add(rs.getInt("Harga"));
            row.add(rs.getInt("Sub_Total"));
            tableData.add(row);
            i++;
        }
        con.tutupKoneksi();
        return tableData;
        }catch(SQLException ex){
            ex.printStackTrace();
            return null;
        } 
}

    
public boolean delete(String Tanggal,Integer KdPenjualan, Integer Kd_Barang, String NamaBarang, Integer Qty, Integer Harga, Integer SubTotal){
    boolean berhasil = false;
    Koneksi con = new Koneksi();
    try{
        con.bukaKoneksi();
        con.preparedStatement = con.dbkoneksi.prepareStatement
        ("delete from detail_penjualan where Kd_Penjualan"+" = ? Kd_Barang"+" = ? Nama_Barang"+" = ? Qty"+" = ? Harga"+"= ? and Sub_Total = ?" );
        con.preparedStatement.setString(1, Tanggal);
        con.preparedStatement.setInt(2, KdPenjualan);
        con.preparedStatement.setInt(3, Kd_Barang);
        con.preparedStatement.setString(4, NamaBarang);
        con.preparedStatement.setInt(5, Qty);
        con.preparedStatement.setInt(6, Harga);
        con.preparedStatement.setInt(7, SubTotal);
        con.preparedStatement.executeUpdate();
           berhasil = true;
        }catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally{
        con.tutupKoneksi();
        return berhasil;
    }
}
    
 public boolean select(int nomor){
     try{
         Koneksi con = new Koneksi();
         con.bukaKoneksi();
         con.statement = con.dbkoneksi.createStatement();
         ResultSet rs = con.statement.executeQuery
        ("select Tanggal, Kd_Penjualan, Kd_Barang, Nama_Barang, Qty, Harga, Sub_Total "+" from detail_penjualan where Kd_Penjualan = "+nomor);
             while (rs.next()){
             this.Tanggal = rs.getString("Tanggal");
             this.KdPenjualan = rs.getInt("Kd_Penjualan");
             this.KdBarang = rs.getInt("Kd_Barang");
             this.NamaBarang = rs.getString("Nama_Barang");
             this.Qty = rs.getInt("Qty");
             this.Harga = rs.getInt("Harga");
             this.SubTotal = rs.getInt("Sub_Total");
             }
         con.tutupKoneksi();
         return true;
       } catch (SQLException ex){
           ex.printStackTrace();
           return false;
       }
 }
 
public int validasiBarang(String NamaBarang){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
           ResultSet rs = con.statement.executeQuery
        ("Select count(*) as jml "+"from detail_penjualan where Nama_Barang = '"+NamaBarang+"'");
            while (rs.next()){
                val = rs.getInt("jml");
        }
        con.tutupKoneksi();
        }catch (SQLException ex){
         ex.printStackTrace();
        }   
        return val;
        }

public Vector Lookup(String fld, String dt){
     
    try{
        Vector tableData =  new Vector();
        Koneksi con = new Koneksi();
        con.bukaKoneksi();
        con.statement = con.dbkoneksi.createStatement();
        ResultSet rs = con.statement.executeQuery
        ("Select Tanggal, Kd_Penjualan, Kd_Barang, Nama_Barang, Qty, Harga, Sub_Total from detail_barang where "+fld+ " like '%"+dt+"%'");
        int i=1;
        while (rs.next()){
            Vector<Object> row = new Vector<Object>();
            row.add(i);
            row.add(rs.getString("Tanggal"));
            row.add(rs.getInt("Kd_Penjualan"));
            row.add(rs.getInt("Kd_Barang"));
            row.add(rs.getString("Nama_Barang"));
            row.add(rs.getInt("Qty"));
            row.add(rs.getInt("Harga"));
            row.add(rs.getInt("Sub_Total"));
            tableData.add(row);
            i++;        }
        con.tutupKoneksi(); return tableData;
    }catch (SQLException ex){
        ex.printStackTrace();   return null;} 
 }



}
