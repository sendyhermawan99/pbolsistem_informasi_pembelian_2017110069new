/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem_informasi_pembelian_2017110069;

import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author SENARIYUS
 */
public class P_Transaksi extends javax.swing.JInternalFrame {
    private DefaultTableModel TabModel;
 // Koneksi con = new Koneksi();
    private Connection conn = new Koneksi().connect();
    Statement statement;
    ResultSet rs;

    
    /**
     * Creates new form P_Transaksi
     */
    public P_Transaksi() {
        initComponents();
        siapIsi(false);
        tombolNormal();
        txstok.setVisible(false);
        txsubtotal.setVisible(false);
        txkodebarang2.setVisible(false);
        txhasilstok.setVisible(false);
        Object header[]={"No.Product","Nama Barang","Harga Jual","Jumlah","Sub Total"};
        TabModel=new DefaultTableModel(null, header);
        tampilkan_data();
        lebarKolom();
        //rata();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        txtanggal12.setDateFormatString(dateFormat.format(cal.getTime()));
    }

    public void rata(){
    
           
            DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
            renderer.setHorizontalAlignment(JLabel.RIGHT);
        
            DefaultTableCellRenderer renderer1 = new DefaultTableCellRenderer();
            renderer1.setHorizontalAlignment(JLabel.CENTER);
            tabeltransaksi.getColumnModel().getColumn(0).setCellRenderer(renderer1);
            tabeltransaksi.getColumnModel().getColumn(2).setCellRenderer(renderer1);
            tabeltransaksi.getColumnModel().getColumn(3).setCellRenderer(renderer1);
            tabeltransaksi.getColumnModel().getColumn(4).setCellRenderer(renderer1);
            
            DefaultTableCellRenderer renderer2 = new DefaultTableCellRenderer();
            renderer2.setHorizontalAlignment(JLabel.LEFT);
            tabeltransaksi.getColumnModel().getColumn(1).setCellRenderer(renderer2);
            
    }
    
    
    
    public void lebarKolom() {
        TableColumn kolom;
        tabelbarangtersedia.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        kolom = tabelbarangtersedia.getColumnModel().getColumn(0);
        kolom.setPreferredWidth(100);
        kolom = tabelbarangtersedia.getColumnModel().getColumn(1);
        kolom.setPreferredWidth(250);
        kolom = tabelbarangtersedia.getColumnModel().getColumn(2);
        kolom.setPreferredWidth(60);
        kolom = tabelbarangtersedia.getColumnModel().getColumn(3);
        kolom.setPreferredWidth(100);
      
       }
 
    
           private void open_db() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/sistem_penjualan_2017110069","root","");
            statement =  (Statement)conn.createStatement();

        }catch (Exception e){
            JOptionPane.showMessageDialog(null,"Koneksi gagal");
            System.out.println(e.getMessage());
        }
}

    private void bersih(){
        txkodetransaksi.setText("");
        txkodebarang.setText("");
        txnamabarang.setText("");
        txharga.setText("");
        txjumlah.setText("0");
        txsubtotal.setText("0");
        txtotal.setText("0");
        txbayar.setText("0");
        txkembalian.setText("");;
    }
    
    private void siapIsi(boolean a){;
        txkodetransaksi.setEnabled(a);
        txkodebarang.setEnabled(a);
        txnamabarang.setEnabled(a);
        txharga.setEnabled(a);
        txjumlah.setEnabled(a);
        txtotal.setEnabled(a);
        txbayar.setEnabled(a);
       txkembalian.setEnabled(a);
    }
    
    private void tombolNormal(){
        bttambah.setEnabled(true);
        btsimpan.setEnabled(false);
        bttambahbarang.setEnabled(false);
//        bthapusbarang.setEnabled(false);
    }
    
    private void otomatis(){
        try {
            open_db();
            String sql="select right (Kode_Transaksi,2)+1 from jual ";
            ResultSet rs=statement.executeQuery(sql);
            if(rs.next()){
                rs.last();
                String no=rs.getString(1);
                while (no.length()<3){
                    no="0"+no;
                    txkodetransaksi.setText("KT"+no);}
                }
            else
            {
                txkodetransaksi.setText("KT001"); 
        }
        } catch (Exception e) 
        {
        }
    }
    
        public void ambildata() {
        try {
            tabeltransaksi.setModel(TabModel);
                String kolom1 = txkodebarang.getText();
                String kolom2 = txnamabarang.getText();
                String kolom3 = txharga.getText();
                String kolom4 = txjumlah.getText();
                String kolom5 = txsubtotal.getText();
                String[] kolom = {kolom1, kolom2, kolom3, kolom4, kolom5};
                TabModel.addRow(kolom);
          }
          catch (Exception ex) {
              JOptionPane.showMessageDialog(null, "Data gagal disimpan");
          }     
    }
    
    private void tampilkan_data() {
        DefaultTableModel tabelmapel = new DefaultTableModel();
        tabelmapel.addColumn("KODE BARANG");
        tabelmapel.addColumn("NAMA BARANG");
        tabelmapel.addColumn("STOK");
        tabelmapel.addColumn("HARGA SATUAN");
        
        try{
            open_db();
             String sql = "select * from barang";
             statement = (com.mysql.jdbc.Statement) conn.createStatement();
            ResultSet rs=statement.executeQuery(sql);
            while (rs.next()) {
                Object[] o =new Object[4];
                o[0] = rs.getString("Kode_barang");
                o[1] = rs.getString("Nama_Barang");
                o[2] = rs.getString("Soh");
                o[3] = rs.getString("Harga");
                
                tabelmapel.addRow(o);
                
            }
           tabelbarangtersedia.setModel(tabelmapel);
        } catch (Exception e) {
        }
}
    
    private void simpan(){
        open_db();
        try {
           Date skrg=new Date();
           SimpleDateFormat frm=new SimpleDateFormat("yyyy-MM-dd");
           String tanggal=frm.format(skrg); 
            
            
            
            int t = tabeltransaksi.getRowCount();
             for(int i=0;i<t;i++)    
            {
            String Kode_Barang=tabeltransaksi.getValueAt(i, 0).toString();
            String Nama_Barang=tabeltransaksi.getValueAt(i, 1).toString();
            int Harga= Integer.parseInt(tabeltransaksi.getValueAt(i, 2).toString());
            int Jumlah= Integer.parseInt(tabeltransaksi.getValueAt(i, 3).toString());
            int Subtotal= Integer.parseInt(tabeltransaksi.getValueAt(i, 4).toString());
         
            String sql ="insert into jual values('"+txkodetransaksi.getText()+"','"+tanggal+"','"
                    +Kode_Barang+"','"+Nama_Barang+"','"+Harga+"','"+Jumlah+"','"+Subtotal+"','"
                    +txtotal.getText()+"','"+txbayar.getText()+"','"+txkembalian.getText()+"')";
            
             statement.executeUpdate(sql);
             
            }         
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "SIMPAN TRANSAKSI PENJUALAN GAGAL");
        perbarui();
        }
    }
    
    private void perbarui(){
        try{
            open_db();
            String sql="update barang set Soh='"+txhasilstok.getText()+"' where Kode_Barang='"+txkodebarang2.getText()+"'";
            statement.executeUpdate(sql);
            //JOptionPane.showMessageDialog(null,"Stok data berhasil","kaset atm",JOptionPane.INFORMATION_MESSAGE);
        } 
        catch(Exception e){
        }
        tampilkan_data();
    }
    

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txkodetransaksi = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txkodebarang2 = new javax.swing.JTextField();
        txhasilstok = new javax.swing.JTextField();
        txstok = new javax.swing.JTextField();
        txsubtotal = new javax.swing.JTextField();
        txkodebarang = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txjumlah = new javax.swing.JTextField();
        txharga = new javax.swing.JTextField();
        txnamabarang = new javax.swing.JTextField();
        bttambahbarang = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabeltransaksi = new javax.swing.JTable();
        bttambah = new javax.swing.JButton();
        btsimpan = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtotal = new javax.swing.JTextField();
        txbayar = new javax.swing.JTextField();
        txkembalian = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelbarangtersedia = new javax.swing.JTable();
        txpencarian = new javax.swing.JTextField();
        txtanggal12 = new com.toedter.calendar.JDateChooser();

        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Transaksi");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(153, 255, 255));
        jPanel1.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanel1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                jPanel1ComponentShown(evt);
            }
        });
        jPanel1.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(102, 102, 255));
        jPanel2.setToolTipText("");
        jPanel2.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanel2.setLayout(null);

        jLabel13.setFont(new java.awt.Font("Georgia", 1, 24)); // NOI18N
        jLabel13.setText("TRANSAKSI");
        jPanel2.add(jLabel13);
        jLabel13.setBounds(460, 10, 160, 28);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 1370, 50);

        jPanel3.setBackground(new java.awt.Color(153, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Transaksi", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 0, 12))); // NOI18N
        jPanel3.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanel3.setLayout(null);

        jLabel2.setText("KODE TRANSAKSI");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel3.add(jLabel2);
        jLabel2.setBounds(20, 30, 110, 14);

        txkodetransaksi.setEditable(false);
        txkodetransaksi.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txkodetransaksi.setDisabledTextColor(new java.awt.Color(51, 51, 255));
        txkodetransaksi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txkodetransaksiActionPerformed(evt);
            }
        });
        jPanel3.add(txkodetransaksi);
        txkodetransaksi.setBounds(130, 30, 140, 20);

        jLabel11.setText("Tanggal");
        jPanel3.add(jLabel11);
        jLabel11.setBounds(300, 30, 80, 14);

        txkodebarang2.setEditable(false);
        jPanel3.add(txkodebarang2);
        txkodebarang2.setBounds(280, 60, 110, 20);

        txhasilstok.setEditable(false);
        txhasilstok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txhasilstokActionPerformed(evt);
            }
        });
        jPanel3.add(txhasilstok);
        txhasilstok.setBounds(400, 60, 60, 20);

        txstok.setEditable(false);
        txstok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txstokActionPerformed(evt);
            }
        });
        jPanel3.add(txstok);
        txstok.setBounds(470, 60, 60, 20);

        txsubtotal.setEditable(false);
        jPanel3.add(txsubtotal);
        txsubtotal.setBounds(540, 60, 70, 20);

        txkodebarang.setEditable(false);
        jPanel3.add(txkodebarang);
        txkodebarang.setBounds(10, 120, 90, 20);

        jLabel5.setText("No.Product");
        jPanel3.add(jLabel5);
        jLabel5.setBounds(20, 100, 100, 14);

        jLabel6.setText("NAMA BARANG");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(230, 100, 120, 14);

        jLabel7.setText("HARGA");
        jPanel3.add(jLabel7);
        jLabel7.setBounds(440, 100, 70, 14);

        jLabel8.setText("JUMLAH");
        jPanel3.add(jLabel8);
        jLabel8.setBounds(540, 100, 80, 14);
        jPanel3.add(txjumlah);
        txjumlah.setBounds(530, 120, 100, 20);

        txharga.setEditable(false);
        txharga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txhargaKeyTyped(evt);
            }
        });
        jPanel3.add(txharga);
        txharga.setBounds(420, 120, 100, 20);

        txnamabarang.setEditable(false);
        txnamabarang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txnamabarangActionPerformed(evt);
            }
        });
        jPanel3.add(txnamabarang);
        txnamabarang.setBounds(110, 120, 300, 20);

        bttambahbarang.setText("+");
        bttambahbarang.setToolTipText("Tambah transaksi");
        bttambahbarang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttambahbarangActionPerformed(evt);
            }
        });
        jPanel3.add(bttambahbarang);
        bttambahbarang.setBounds(640, 120, 100, 23);

        tabeltransaksi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "KODE BARANG","NAMA BARANG","STOK","HARGA SATUAN"
            }
        ));
        tabeltransaksi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabeltransaksiMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabeltransaksi);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(10, 150, 740, 200);

        bttambah.setText("Tambah");
        bttambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttambahActionPerformed(evt);
            }
        });
        jPanel3.add(bttambah);
        bttambah.setBounds(20, 360, 110, 50);

        btsimpan.setText("Simpan");
        btsimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btsimpanActionPerformed(evt);
            }
        });
        jPanel3.add(btsimpan);
        btsimpan.setBounds(140, 360, 110, 50);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setText("Total");
        jPanel3.add(jLabel9);
        jLabel9.setBounds(540, 360, 29, 14);

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText("Bayar");
        jPanel3.add(jLabel10);
        jLabel10.setBounds(540, 390, 33, 14);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText(" Kembali");
        jPanel3.add(jLabel4);
        jLabel4.setBounds(540, 420, 48, 14);

        txtotal.setEditable(false);
        txtotal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtotalActionPerformed(evt);
            }
        });
        txtotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtotalKeyTyped(evt);
            }
        });
        jPanel3.add(txtotal);
        txtotal.setBounds(600, 360, 150, 20);

        txbayar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txbayar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txbayarKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txbayarKeyTyped(evt);
            }
        });
        jPanel3.add(txbayar);
        txbayar.setBounds(600, 390, 150, 20);

        txkembalian.setEditable(false);
        txkembalian.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txkembalian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txkembalianKeyTyped(evt);
            }
        });
        jPanel3.add(txkembalian);
        txkembalian.setBounds(600, 420, 150, 20);

        tabelbarangtersedia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "No.Product", "Nama Barang", "Soh", "Harga"
            }
        ));
        tabelbarangtersedia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelbarangtersediaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tabelbarangtersedia);

        jPanel3.add(jScrollPane2);
        jScrollPane2.setBounds(770, 90, 550, 402);

        txpencarian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txpencarianActionPerformed(evt);
            }
        });
        txpencarian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txpencarianKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txpencarianKeyTyped(evt);
            }
        });
        jPanel3.add(txpencarian);
        txpencarian.setBounds(770, 40, 550, 40);
        jPanel3.add(txtanggal12);
        txtanggal12.setBounds(390, 30, 150, 20);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(10, 60, 1340, 490);

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 2, 1370, 590));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel1ComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jPanel1ComponentShown
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel1ComponentShown

    private void txkodetransaksiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txkodetransaksiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txkodetransaksiActionPerformed

    private void txhasilstokActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txhasilstokActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txhasilstokActionPerformed

    private void txstokActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txstokActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txstokActionPerformed

    private void txhargaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txhargaKeyTyped
        // TODO add your handling code here:
        char karakter = evt.getKeyChar();
        if (!(Character.isDigit(karakter) || karakter==KeyEvent.VK_BACK_SPACE))
        {
            evt.consume();
        }
    }//GEN-LAST:event_txhargaKeyTyped

    private void txnamabarangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txnamabarangActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txnamabarangActionPerformed

    private void bttambahbarangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttambahbarangActionPerformed
        // TODO add your handling code here:

        int harga=Integer.parseInt(txharga.getText());
        int jml=Integer.parseInt(txjumlah.getText());
        int stok=Integer.parseInt(txstok.getText());
        int total=Integer.parseInt(txtotal.getText());

        if(jml>stok){
            JOptionPane.showMessageDialog(null, "Stok barang tidak mencukupi");
        }else{

            int subtot=harga*jml;
            txsubtotal.setText(Integer.toString(subtot));

            int hasilstok=stok-jml;
            txhasilstok.setText(Integer.toString(hasilstok));

            int totbay=total+(harga*jml);
            txtotal.setText(Integer.toString(totbay));

            ambildata();

            txkodebarang.setText("");
            txnamabarang.setText("");
            txharga.setText("");
            txjumlah.setText("");
            // stockTF.setText("");
        }
    }//GEN-LAST:event_bttambahbarangActionPerformed

    private void tabeltransaksiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabeltransaksiMouseClicked
        // TODO add your handling code here:
        tabeltransaksi.setRowSelectionAllowed(true);
        int a = tabeltransaksi.getSelectedRow();
        String kolom1 = tabeltransaksi.getValueAt(a,1).toString();
        String kolom2 = tabeltransaksi.getValueAt(a,2).toString();
        String kolom3 = tabeltransaksi.getValueAt(a,3).toString();
        String kolom4 = tabeltransaksi.getValueAt(a,4).toString();
        String kolom5 = tabeltransaksi.getValueAt(a,5).toString();
        String kolom6 = tabeltransaksi.getValueAt(a,6).toString();
        String kolom7 = tabeltransaksi.getValueAt(a,7).toString();
        String kolom8 = tabeltransaksi.getValueAt(a,8).toString();
    }//GEN-LAST:event_tabeltransaksiMouseClicked

    private void bttambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttambahActionPerformed
        // TODO add your handling code here:
        if(bttambah.getText().equalsIgnoreCase("tambah")){
            bttambah.setText("Refresh");
            siapIsi(true);
            bersih();
            otomatis();

            txkodetransaksi.setEnabled(false);
            btsimpan.setEnabled(true);
            bttambahbarang.setEnabled(true);
            //            bthapusbarang.setEnabled(true);

        }else{
            bersih();
            siapIsi(false);
            bttambah.setText("Tambah");
            TabModel.getDataVector().removeAllElements();
            TabModel.fireTableDataChanged();
            tombolNormal();
        }
    }//GEN-LAST:event_bttambahActionPerformed

    private void btsimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btsimpanActionPerformed
        // TODO add your handling code here:
        if(txkodetransaksi.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Lengkapi inputan penjualan barang");
        } else{
            simpan();

            //int pesan=JOptionPane.showConfirmDialog(null, "Cetak Kwitansi Nota","Cetak",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);

            //if(pesan==JOptionPane.YES_OPTION){
                //                cetak_nota();
                //}else {
                JOptionPane.showMessageDialog(null, "Simpan Transaksi Berhasil");
            }
            bersih();
            perbarui();
            tampilkan_data();

    }//GEN-LAST:event_btsimpanActionPerformed

    private void txtotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtotalActionPerformed

    private void txtotalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotalKeyTyped
        // TODO add your handling code here:
        //validasi data harus angka atau tombol backspace
        char karakter = evt.getKeyChar();
        if (!(Character.isDigit(karakter) || karakter==KeyEvent.VK_BACK_SPACE))
        {
            evt.consume();
        }
    }//GEN-LAST:event_txtotalKeyTyped

    private void txbayarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txbayarKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            int total=Integer.parseInt(txtotal.getText());
            int bayar=Integer.parseInt(txbayar.getText());
            if(bayar<total){
                JOptionPane.showMessageDialog(null, "Jumlah bayar tidak mencukupi");
                txbayar.requestFocus();
            } else{
                int kembali=bayar-total;
                txkembalian.setText(Integer.toString(kembali));
            }
        }
    }//GEN-LAST:event_txbayarKeyPressed

    private void txbayarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txbayarKeyTyped
        // TODO add your handling code here:
        //validasi data harus angka atau tombol backspace
        char karakter = evt.getKeyChar();
        if (!(Character.isDigit(karakter) || karakter==KeyEvent.VK_BACK_SPACE || karakter==KeyEvent.VK_ENTER))
        {
            evt.consume();
        }
    }//GEN-LAST:event_txbayarKeyTyped

    private void txkembalianKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txkembalianKeyTyped
        // TODO add your handling code here:
        //validasi data harus angka atau tombol backspace
        char karakter = evt.getKeyChar();
        if (!(Character.isDigit(karakter) || karakter==KeyEvent.VK_BACK_SPACE))
        {
            evt.consume();
        }
    }//GEN-LAST:event_txkembalianKeyTyped

    private void tabelbarangtersediaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelbarangtersediaMouseClicked
        int baris = tabelbarangtersedia.getSelectedRow();
        txkodebarang.setText(tabelbarangtersedia.getModel().getValueAt(baris, 0).toString());
        txkodebarang2.setText(tabelbarangtersedia.getModel().getValueAt(baris, 0).toString());
        txnamabarang.setText(tabelbarangtersedia.getModel().getValueAt(baris, 1).toString());
        txstok.setText(tabelbarangtersedia.getModel().getValueAt(baris, 2).toString());
        txharga.setText(tabelbarangtersedia.getModel().getValueAt(baris, 3).toString());
    }//GEN-LAST:event_tabelbarangtersediaMouseClicked

    private void txpencarianActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txpencarianActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txpencarianActionPerformed

    private void txpencarianKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txpencarianKeyPressed
       // lebarKolom();
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            Object header[]={"No.Product","Nama Barang","Soh","Harga Satuan"};
            DefaultTableModel data=new DefaultTableModel(null,header);
            tabelbarangtersedia.setModel(data);

            String sql="Select * from barang where Kd_Barang like '%" +
            txpencarian.getText() + "%'" + "or Nama_Barang like '%" + txpencarian.getText()+ "%'";
            try {
                ResultSet rs=statement.executeQuery(sql);
                while (rs.next())
                {
                    String kolom1=rs.getString(1);
                    String kolom2=rs.getString(2);
                    String kolom3=rs.getString(7);
                    String kolom4=rs.getString(8);
                    String kolom[]={kolom1,kolom2,kolom3,kolom4};
                    data.addRow(kolom);
                }
            } catch (SQLException e) {
            }
        }
    }//GEN-LAST:event_txpencarianKeyPressed

    private void txpencarianKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txpencarianKeyTyped
    lebarKolom();
    }//GEN-LAST:event_txpencarianKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btsimpan;
    private javax.swing.JButton bttambah;
    private javax.swing.JButton bttambahbarang;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabelbarangtersedia;
    private javax.swing.JTable tabeltransaksi;
    private javax.swing.JTextField txbayar;
    private javax.swing.JTextField txharga;
    private javax.swing.JTextField txhasilstok;
    private javax.swing.JTextField txjumlah;
    private javax.swing.JTextField txkembalian;
    private javax.swing.JTextField txkodebarang;
    private javax.swing.JTextField txkodebarang2;
    private javax.swing.JTextField txkodetransaksi;
    private javax.swing.JTextField txnamabarang;
    private javax.swing.JTextField txpencarian;
    private javax.swing.JTextField txstok;
    private javax.swing.JTextField txsubtotal;
    private com.toedter.calendar.JDateChooser txtanggal12;
    private javax.swing.JTextField txtotal;
    // End of variables declaration//GEN-END:variables
}
