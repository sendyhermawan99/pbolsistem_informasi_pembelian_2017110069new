/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem_informasi_pembelian_2017110069;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

/**
 *
 * @author SENARIYUS
 */
public class DbLogIn {
    
    int Id;
    String  UserName;
    String Password;
    String Level;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getLevel() {
        return Level;
    }

    public void setLevel(String Level) {
        this.Level = Level;
    }
    
    
        public boolean insert(){
        boolean berhasil=false;
        Koneksi con=new Koneksi();
        try {
            con.bukaKoneksi();
            con.preparedStatement = con.dbkoneksi.prepareStatement
        ("Insert into login"+"(Id, UserName, Password, Level) values (?,?,?,?)");
                     con.preparedStatement.setInt(1, this.Id);
                     con.preparedStatement.setString(2, this.UserName);
                     con.preparedStatement.setString(3, this.Password);
                     con.preparedStatement.setString(4, this.Level);
                     con.preparedStatement.executeUpdate();
         berhasil=true;
        } catch(Exception e){
            e.printStackTrace();
            berhasil=false;
        } finally {
          con.tutupKoneksi();  
          return berhasil;          
        }
    }

    
          
              public Vector Load(){
        try{
            Vector tableData = new Vector();
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
            ResultSet rs = con.statement.executeQuery
        ("Select Id, UserName, Password, Level from login");
        int i=1; 
        while (rs.next()){
            Vector <Object> row = new Vector <Object>();
     
            row.add(i);
            row.add(rs.getInt("Id"));
            row.add(rs.getString("UserName"));
            row.add(rs.getString("Password"));
            row.add(rs.getString("Level"));
            tableData.add(row);
            i++;
        }
        con.tutupKoneksi();
        return tableData;
        }catch(SQLException ex){
            ex.printStackTrace();
            return null;
        } 
}

public boolean delete(String ID){
    boolean berhasil = false;
    Koneksi con = new Koneksi();
    try{
        con.bukaKoneksi();
        con.preparedStatement = con.dbkoneksi.prepareStatement
        ("delete from login where Id"+" = UserName"+" = Password"+" = Level"+" = ");
        con.preparedStatement.setString(1, ID);
        con.preparedStatement.executeUpdate();
           berhasil = true;
        }catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally{
        con.tutupKoneksi();
        return berhasil;
    }
}

              
 public boolean select(int nomor){
     try{
         Koneksi con = new Koneksi();
         con.bukaKoneksi();
         con.statement = con.dbkoneksi.createStatement();
         ResultSet rs = con.statement.executeQuery
        ("select Id, UserName, Password, Level "+" from login where Id = "+nomor);
             while (rs.next()){
             this.Id = rs.getInt("Id");
             this.UserName = rs.getString("UserName ");
             this.Password = rs.getString("Password");
             this.Level = rs.getString("Level");
             }
         con.tutupKoneksi();
         return true;
       } catch (SQLException ex){
           ex.printStackTrace();
           return false;
       }
 }
    
 public int validasiBarang(String User){
        int val = 0;
        try{
            Koneksi con = new Koneksi();
            con.bukaKoneksi();
            con.statement = con.dbkoneksi.createStatement();
           ResultSet rs = con.statement.executeQuery
        ("Select count(*) as jml "+"from login where UserName = '"+User+"'");
            while (rs.next()){
                val = rs.getInt("jml");
        }
        con.tutupKoneksi();
        }catch (SQLException ex){
         ex.printStackTrace();
        }   
        return val;
        }


 public Vector Lookup(String fld, String dt){
     
    try{
        Vector tableData =  new Vector();
        Koneksi con = new Koneksi();
        con.bukaKoneksi();
        con.statement = con.dbkoneksi.createStatement();
        ResultSet rs = con.statement.executeQuery
        ("Select Id, Soh from login where "+fld+ " like '%"+dt+"%'");
        int i=1;
        while (rs.next()){
            Vector<Object> row = new Vector<Object>();
            row.add(i);
            row.add(rs.getInt("Id"));
            row.add(rs.getString("UserName"));
            row.add(rs.getString("Password"));
            row.add(rs.getString("Level"));
            tableData.add(row);
            i++;
        }
        con.tutupKoneksi(); return tableData;
    }catch (SQLException ex){
        ex.printStackTrace();   return null;} 
 }

public boolean update(int id){
        boolean berhasil = false;
        Koneksi con = new Koneksi();
        try{
            con.bukaKoneksi();
            con.preparedStatement = con.dbkoneksi.prepareStatement
        ("update login set UserName=? where Id = ?");
                     con.preparedStatement.setInt(1, Id);
                     con.preparedStatement.setString(2, UserName);
                     con.preparedStatement.setString(3, Password);
                     con.preparedStatement.setString(4, Level);
                     con.preparedStatement.executeUpdate();
                    // con.preparedStatement.executeUpdate();
             
                     berhasil = true;
                     
        } catch (Exception e){
            e.printStackTrace();
            berhasil = false;
        } finally {
            con.tutupKoneksi();
            return berhasil;
        }
    }

    
    
}
