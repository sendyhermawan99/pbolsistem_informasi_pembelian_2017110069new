package sistem_informasi_pembelian_2017110069;

import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class M_Supliyer extends javax.swing.JInternalFrame {

        DbSupliyer supliyer = new DbSupliyer();
        Tambah_Supliyer TSupliyer = new Tambah_Supliyer(null, true);

    
    public M_Supliyer() {
        initComponents();
        tampilkanDataAll();
    }

    
    private void klikDelete(){
        if (jtbSupliyer.getSelectedRow()<0) 
            JOptionPane.showMessageDialog(this, "Pilih data dulu");
        else {
            int konfirm = JOptionPane.showConfirmDialog(null, "Hapus data?");
            if (konfirm==JOptionPane.YES_OPTION){
                String kode = String.valueOf(jtbSupliyer.getValueAt(jtbSupliyer.getSelectedRow(), 1));
                supliyer.delete(kode);
                tampilkanDataAll();
            }
        }
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtbSupliyer = new javax.swing.JTable();
        btnHapus1 = new javax.swing.JButton();
        btnTambah1 = new javax.swing.JButton();
        btnResresh = new javax.swing.JButton();
        btnEdit1 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Master Supliyer");
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(153, 255, 255));
        jPanel1.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(102, 102, 255));
        jPanel2.setToolTipText("");
        jPanel2.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanel2.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Georgia", 1, 24)); // NOI18N
        jLabel1.setText("MASTER DATA SUPLIYER");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(360, 10, 340, 28);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 1360, 50);

        jPanel3.setBackground(new java.awt.Color(153, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Data Supliyer", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 0, 12))); // NOI18N
        jPanel3.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        jPanel3.setLayout(null);

        jtbSupliyer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "No", "Kode Supliyer", "Nama Supliyer", "Telephone", "Alamat"
            }
        ));
        jScrollPane2.setViewportView(jtbSupliyer);

        jPanel3.add(jScrollPane2);
        jScrollPane2.setBounds(10, 20, 1310, 490);

        btnHapus1.setText("Hapus");
        btnHapus1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapus1ActionPerformed(evt);
            }
        });
        jPanel3.add(btnHapus1);
        btnHapus1.setBounds(210, 520, 90, 40);

        btnTambah1.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnTambah1.setText("Tambah");
        btnTambah1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambah1ActionPerformed(evt);
            }
        });
        jPanel3.add(btnTambah1);
        btnTambah1.setBounds(10, 520, 90, 40);

        btnResresh.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnResresh.setText("Refresh");
        btnResresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResreshActionPerformed(evt);
            }
        });
        jPanel3.add(btnResresh);
        btnResresh.setBounds(310, 520, 90, 40);

        btnEdit1.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        btnEdit1.setText("Edit");
        btnEdit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEdit1ActionPerformed(evt);
            }
        });
        jPanel3.add(btnEdit1);
        btnEdit1.setBounds(110, 520, 90, 40);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(10, 60, 1330, 570);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1350, 640);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnHapus1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapus1ActionPerformed
        // TODO add your handling code here:
        klikDelete();
    }//GEN-LAST:event_btnHapus1ActionPerformed

    private void btnTambah1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambah1ActionPerformed
        // TODO add your handling code here:
        Tambah_Supliyer input = new Tambah_Supliyer(null,true);
        if (input.execute(0))
        tampilkanDataAll();
    }//GEN-LAST:event_btnTambah1ActionPerformed

    private void btnResreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResreshActionPerformed
        // TODO add your handling code here:
        tampilkanDataAll();
    }//GEN-LAST:event_btnResreshActionPerformed

    private void btnEdit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEdit1ActionPerformed
        // TODO add your handling code here:
        Tambah_Supliyer input = new Tambah_Supliyer(null,true);
        int dSupliyer = Integer.valueOf(String.valueOf(jtbSupliyer.getValueAt(jtbSupliyer.getSelectedRow(), 1)));
        input.execute(dSupliyer);
        tampilkanDataAll();
    }//GEN-LAST:event_btnEdit1ActionPerformed
public void tampilkanDataAll(){
    Vector<String> tableHeader = new Vector<String>();
    tableHeader.add("No.");
    tableHeader.add("Kode Supliyer");
    tableHeader.add("Nama Supliyer");
    tableHeader.add("Telephone");
    tableHeader.add("Alamat");
    
    Vector tableData = supliyer.Load();
    if (tableData!=null){
            jtbSupliyer.setModel(new DefaultTableModel(tableData,tableHeader){
             // agar tidak bisa di edit datanya
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false; //To change body of generated methods, choose Tools | Templates.
                }
            });
           
            DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
            renderer.setHorizontalAlignment(JLabel.RIGHT);
            
            DefaultTableCellRenderer renderer1 = new DefaultTableCellRenderer();
            renderer1.setHorizontalAlignment(JLabel.CENTER);
            jtbSupliyer.getColumnModel().getColumn(2).setCellRenderer(renderer1);
            jtbSupliyer.getColumnModel().getColumn(1).setCellRenderer(renderer1);
            jtbSupliyer.getColumnModel().getColumn(0).setCellRenderer(renderer1);
     
//ngilangin colloumn   
            
           //jtBarang.getColumnModel().getColumn(0).setMinWidth(0);
            //jtBarang.getColumnModel().getColumn(0).setMaxWidth(0);
            
// Mengurut kan data
          jtbSupliyer.setAutoCreateRowSorter(true);
          TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(jtbSupliyer.getModel());
          sorter.setSortable(0, false);
          sorter.addRowSorterListener(new RowSorterListener(){
              
              public void sorterChanged(RowSorterEvent e){
                  int colNomor = 0 ;
                  for (int i=0;i<jtbSupliyer.getRowCount();i++){
                      jtbSupliyer.setValueAt(i+1,i, colNomor);
                  }
              }
          });
          
          jtbSupliyer.setRowSorter(sorter);
          jtbSupliyer.grabFocus();
    }

    }

    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEdit1;
    private javax.swing.JButton btnHapus1;
    private javax.swing.JButton btnResresh;
    private javax.swing.JButton btnTambah1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jtbSupliyer;
    // End of variables declaration//GEN-END:variables
}
